import Constants as keys
from telegram.ext import *
import telegram
import logging
import Scraping as scraping

print("Bot initiated...")
started = False
lastSent = ""
SECONDS = 1800

def start_command(update, context):
    global started
    if not started:
        update.message.reply_text("Bot partito. Ogni mezz'ora controllerà le aziende presenti nel database e manderà aggiornamenti solo se ci sono novità. Dai /check per forzare il controllo.")
        context.job_queue.run_repeating(callback_check, interval = SECONDS, first = 1, context=update.message.chat_id)
        started = True
    else:
        update.message.reply_text("Bot già partito.")


def check_command(update, context):
    print("Checking companies")
    update.message.reply_text(scraping.main())

def callback_check(context):
    global lastSent
    chat_id = context.job.context
    print("Checking companies (every 6 hours)")
    recentQuery = scraping.main()
    if recentQuery != lastSent:
        context.bot.send_message(chat_id = chat_id, text=recentQuery)
        lastSent = recentQuery
    else:
        print("Non ci sono aggiornamenti.")

def error(update, context):
    print("error")

def main(): 
    updater = Updater(keys.API_KEY, use_context=True)
    dpatcher = updater.dispatcher

    dpatcher.add_handler(CommandHandler("start", start_command))
    dpatcher.add_handler(CommandHandler("check", check_command))
    dpatcher.add_error_handler(error)

    updater.start_polling(drop_pending_updates=False)
    updater.idle()

main()