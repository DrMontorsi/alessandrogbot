from bs4 import BeautifulSoup
import requests
import Constants as keys

def check_company(URL, nameToCheck):
    r = requests.get(URL)
    soup = BeautifulSoup(r.text, 'html.parser')
    results = soup.find_all('span' , {'class' : 'titre'})
    COMPANY_FOUND = False
    for company in results:
        COMPANY_FOUND = COMPANY_FOUND or company.getText().lower() == nameToCheck.lower()
    if COMPANY_FOUND:
        return "PRESENTE"
    else:
        return "ASSENTE"

def main():
    outputString = ""
    for i in range(3):
        outputString += keys.COMPANY_TOCHECK[i] + ": " + check_company(keys.URL[i], keys.COMPANY_TOCHECK[i]) + "\n"
    return outputString


